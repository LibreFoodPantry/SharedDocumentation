# Education

## Want to get involved?

[Contact us](https://librefoodpantry.org/#/overview/communication/) on Discord or by email and let's talk!

## Why Work With LibreFoodPantry?

* Do good for your community by helping to provide free and open-source software to your local food pantry.
* Faculty: enrich your students' learning by engaging them in an open-source project.
* Students: gain experience developing software, exercise and improve valued soft-skills, and build your resume.

## Who Else is Working with LibreFoodPantry?

### Nassau Community College

Client

* NEST

Faculty

* Darci Burdge
* Lori Postner

Courses

* Mobile Application Development

Projects and Modules

* NCC-NEST-Registrations
* NEST

### Western New England University

Client

* BEAR-Necessities-Market

Faculty

* Heidi Ellis
* Stoney Jackson
* Robert Walz

Courses

* Software Engineering
* Computer Science Capstone
* Human-Computer Interaction

Projects and Modules

* BEAR-Necessities-Market
* OrderModule-bnm
* UserModule-bnm

### Worcester State University

Client

* Theas Pantry

Faculty

* Karl Wurst

Courses

* Capstone

Projects and Modules

* Theas-Pantry
* FoodKeeper-API
* InventoryModule-tp
* VisitModule-tp


## Related Work

* [[PDF]](https://librefoodpantry.org/education/papers/SIGCSE2020-paper.pdf) Karl R. Wurst, Christopher Radkowski, Stoney Jackson, Heidi J. C. Ellis, Darci Burdge, and Lori Postner. 2020. **LibreFoodPantry: Developing a Multi-Institutional, Faculty-Led, Humanitarian Free and Open Source Software Community.** In Proceedings of the 51st ACM Technical Symposium on Computer Science Education (SIGCSE ’20). Association for Computing Machinery, New York, NY, USA, 441–447. DOI:https://doi.org/10.1145/3328778.3366929

## Links

* [foss2serve.org](http://foss2serve.org/index.php/Main_Page) and [HFOSS.org](http://hfoss.org)
  * Community of instructors teaching HFOSS.
  * Sign up for and attend a POSSE and get more experience with HFOSS.
  * Find activities that you can use with your classes to engage students in HFOSS.
* [TeachingOpenSource](http://teachingopensource.org/)
  * Join a larger community of faculty who teach and teach with Open Source.

---
Copyright &copy; 2020 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
