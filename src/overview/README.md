# LibreFoodPantry

<img src="_media/lfp-logo.svg" height="450" alt="Libre Food Pantry Logo" />

> Check it out! We're working on a [new website](http://librefoodpantry.gitlab.io/website/)! It's not done yet. And until it is, this is the official LibreFoodPantry website. If you want to give us feedback about the new site, [please leave a comment on this issue](https://gitlab.com/LibreFoodPantry/website/-/issues/1).

We are a community of software developers and clients that builds and maintains free and open-source software to support food pantries.

Currently we have built prototypes of specific features for [specific clients](/projects/), but as of yet no software has been deployed for a client. This means we are in the very early phases of development and much about our software, processes, and tools are in flux. Please be prepared for change.

That said, some things are mostly stable. We have a [Code of Conduct](/overview/code-of-conduct/) that all community members are expected to uphold in all their community interactions. We have a [mission statement](https://librefoodpantry.org/#/overview/about/?id=mission) that helps scope our efforts. We have a [set of values](https://librefoodpantry.org/#/overview/about/?id=values) that help provide a common philosophy around developing software and help us when making decisions. We license all our code under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and all other content under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). We have a [Coordinating Committee](https://librefoodpantry.org/#/overview/about/?id=governance) that meets weekly to coordinate our efforts. All of our development efforts are housed within the [LibreFoodPantry group and subgroups on GitLab](https://gitlab.com/LibreFoodPantry), and we make extensive use of issue boards in groups and projects to manage meetings and coordinate work. We have a [LibreFoodPantry Discord server](https://librefoodpantry.org/#/overview/communication/?id=discord). We have a [user story map](https://librefoodpantry.org/#/projects/StoryMap/) that aggregates features needed by one or more of our clients.

If you have a question, are interested in becoming a client, or are interested in helping out in the development effort, please start a conversation with us by either introducing yourself on our [Discord server](https://librefoodpantry.org/#/overview/communication/?id=discord), [opening an issue](https://gitlab.com/groups/LibreFoodPantry/-/issues) on GitLab, or [sending an email](mailto:LibreFoodPantry@googlegroups.com) to the Coordinating Committee.

---
Copyright &copy; 2020 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
