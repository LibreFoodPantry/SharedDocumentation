# Issue Tracker

## Issues

Most of the communication and work for LibreFoodPantry projects should be done
through the
[LFP issue tracker](https://gitlab.com/groups/LibreFoodPantry/-/issues)
by creating, commenting, and working on issues. Using the issue tracker helps
us to maintain a history of what work has and is being done, avoids redundancy,
keeps everyone on the same page, and promotes transparency since everything is
publicly viewable. New issues can easily be created at any time, so if you see
something that needs one, feel free to create one.

## Labels

Labels are a valuable tool that help us categorize issues into different types,
display their current progress in our workflow, show which shops and teams are
working on them, and other useful information.

See the
[LibreFoodPantry Labels page](https://gitlab.com/groups/LibreFoodPantry/-/labels)
for a list of our
current labels and their descriptions.  Note that these labels are applicable
to all LFP projects, shops can create their own labels that better suit their
specific needs (an example would be having a label for a specific development
platform `platform:android`).

Many of our labels are [scoped labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels-premium). Scoped labels have two colons and have the form `key::value`. Each issue can only have one `value` for each `key`. For example, given scoped labels `type::bug` and `type::feature`, an issue can only be labeled with one of these labels.


### `type::*`

Example: `type::bug`

Type labels are used to categorize issues. Each category is different and affects how an issue will be handled. For example: reporting a bug is handled differently from bringing up a topic for discussion. Since type labels are scope labels, each issue can have only one type.


### `flow::*`

Example: `flow::backlog`

Flow labels are used to show the current working status of an issue. As an issue progresses through our workflow from being created and opened to finished and closed, its flow label should be updated according to its current progress as it is being worked on. Creating an issue board with columns made up of flow labels helps us to visualize what issues are currently being worked on and where they are in our workflow as they move from being opened to closed. Since we are using scoped labels, each issue can only have one flow label at a time.

### Shop

Example: `shop-nest`

Shop labels have the form `shop:name`. They are used to mark issues the shop is tracking. Shop labels can be used to create a shop board that only shows issues that have the shop's label. Note that shop labels are not scoped. This means that more than one shop may track an issue.


### Team

Example: `shop-nest-green`

Team labels are constructed from their shop's label. Team labels are used to mark issues that a team is tracking. Note that team labels are not scoped. This means that more than one team may track an issue.

---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
