# Becoming a Shop Manager

To become a shop manager, petition the coordinating committee for membership.

**Shop Manager Petition contents**
* Full name
* Email
* Organization
* List of the project(s) that they propose to work on.
* The composition, structure, and organization of the shop (number of developers, experience of developers, and frequency of meetings)
* Agree to uphold the values of LibreFoodPantry
* Agree to regularly attend coordinating committee meetings
* Agree to join and participate in community communication channels; currently:
    * librefoodpantry-coordinating-committee@googlegroups.com
    * librefoodpantry@googlegroups.com
    * LibreFoodPantry Discord server
* If the existing meeting time for the coordinating committee does not work for the individual, a list of days and times that would.

The coordinating committee reviews the petition and determines whether to
appoint the applicant as a shop manager.


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
