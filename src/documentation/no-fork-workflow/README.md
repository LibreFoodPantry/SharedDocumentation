# No-Fork Workflow

The workflow described in the Developers' Guide and the Shop Managers' Guide use a fork. This document describes a different workflow that does not use a fork. Check with you shop manager to find out which workflow your shop is using.


## Setup shop

1. Shop manager, create a shop group for your group.
2. Shop manager, add shop members to your shop group.
3. Shop manager, add the shop group as `developers` to the projects and/or groups that your shop will work on.
4. Shop manager, create a shop board, team boards, epics, and/or milestones as needed for your shop.


## Setup development environment

1. Each shop member, clone locally each project you will be working on.


## Prepare to work on an issue

1. One shop member, claim the issue.
2. The same member, create and publish an issue branch.
3. The same member, create a WIP merge request back to master.

## Work on issue

1. All, checkout issue branch.
2. Make, test, stage, commit, pull (and possibly resolve conflicts), and push changes until ready for a review.

## Review and merge work

1. Remove WIP and ping reviewer(s) to request a review.
2. Reviewer(s) review merge request.
3. If changes are requested return to work on issue.
4. Reviewer(s) ping maintainer to merge.
5. Maintainer merges.

## Cleanup

1. One shop member, delete remote branch
2. All
    1. Checkout master and pull
    2. Delete issue branch locally
    3. pull with --prune
3. For each other issue branch, have one shop member update the feature branch as follows
    1. Checkout and pull issue branch.
    2. Merge master into issue branch, testing and resolving any conflicts.
    3. Pull and then push issue branch.
4. Others checkout and pull feature branches.


---
Copyright &copy; 2019 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
