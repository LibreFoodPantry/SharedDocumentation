SharedDocumentation
===================

This is where documentation that is common across all LFP projects is stored.

This project is a LibreFoodPantry project. As such, we share its policies, practices, and infrastructure. Please visit LibreFoodPantry.org for more information.

- Source code licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)
- Content licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
- [Code of Conduct](https://librefoodpantry.org/#/overview/code-of-conduct/)
- [Discord server](https://discord.gg/PRth8YK)
- [Source code and issue tracker](https://gitlab.com/LibreFoodPantry/SharedDocumentation)
- [What's New](./CHANGELOG.md)


File structure
--------------

- src/ contains the shared documentation for all LFP projects.
- docs/ contains documentation about this project.


---

Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
