# SharedDocumentation

Common documentation for LibreFoodPantry projects.

## Documentation about this project

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/SharedDocumentation/)
- [Edit on GitLab](docs) (in `docs/` directory)

## The Documentation Shared Among Projects

- [Browse on librefoodpantry.org](https://librefoodpantry.org/)
- [Edit on GitLab](src) (in `src/` directory)

---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
